//
//  ViewController.swift
//  contatos_cauedias
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit
struct Contato {
    let nome : String
    let email : String
    let endereco: String
    let numero : String
    
    
}
class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        cell.numero.text = contato.numero
        
        return cell
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        listaDeContatos.append(Contato(nome: "contato 1", email: "contato1@gmail.com", endereco: "Rua dos bobos 0", numero: "(11) 98989-1234"))
        
        listaDeContatos.append(Contato(nome: "contato 2", email: "contato2@gmail.com", endereco: "Rua dos bobos 1", numero: "(11) 98989-1235"))
        
        listaDeContatos.append(Contato(nome: "contato 3", email: "contato3@gmail.com", endereco: "Rua dos bobos 2", numero: "(11) 98989-1236"))
        // Do any additional setup after loading the view.
    }


}

